import React from "react";
import "./About.css";

const About = ({ about }) => {
  return (
    <div>
      <div className="about card">
        {about.map((item, indice, array) => {
          return (
            <div key={JSON.stringify(item)}>
              <p className="name">{indice+1} 📕 {item.name}</p>
              <p>{item.where}</p>
              <p>{item.date}</p>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default About;