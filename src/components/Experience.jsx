import React from "react";
import "./Experience.css";

const Experience = ({ experience }) => {
  return (
    <div>
      <div className="experience card">
        {experience.map((item, indice, array) => {
          return (
            <div key={JSON.stringify(item)}>
              <p className="name">{indice+1} 📕 {item.name}</p>
              <p>{item.where}</p>
              <p>{item.description}</p>
              <p>{item.date}</p>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default Experience;