import React from "react";
import "./More.css";

const More = ({ languages, habilities, volunteer }) => {
  return (
    <div>
      <div className="more card">
        {Object.keys(languages).map((item, indice, array) => {
          return (
            <div>
              <p className="name">{item} - {languages[item]}</p>
            </div>
          );
        })}
      </div>
       <div className="more card">
        {volunteer.map((item, indice, array) => {
          return (
            <div key={JSON.stringify(item)}>
              <p className="name">{indice+1} 📕 {item.name}</p>
              <p>{item.where}</p>
              <p>{item.description}</p>
            </div>
          );
        })}
      </div>
      <div className="more card">
        {habilities.map((item, indice, array) => {
          return (
            <div key={JSON.stringify(item)}>
              <p className="name">{item} </p>
            </div>
          );
        })}
      </div> 
    </div>
  );
};

export default More;