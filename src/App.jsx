import "./app.css";
import { useState } from "react";
import { Hero, About, Education, Experience, More  } from "./components"
import { CV } from "./CV/CV";

const { hero, education, experience, languages, habilities, volunteer } = CV;

function App() {
  const [showEducation, setShowEducation] = useState(true);
  return (
    <div className="app">
            <button
              className="custom-btn btn-4"
              onClick={() => setShowEducation(true)}
            >
              Education
            </button>
            <button
              className="custom-btn btn-4"
              onClick={() => setShowEducation(false)}
            >
              Experience
            </button>

            <div>
              {showEducation ? (
                <Education education={education} />
              ) : (
                <Experience experience={experience} />
              )}
            </div>

       <Hero hero={hero} />
       <About about={hero.aboutMe} /> 
	       <More
        languages={languages}
        habilities={habilities}
        volunteer={volunteer}
	      /> 
    </div>
  );
}

export default App;
